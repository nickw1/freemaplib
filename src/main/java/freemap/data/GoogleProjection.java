package freemap.data;



public class GoogleProjection extends SimpleProjection implements Projection {
    
    public static final double EARTH = 40075016.68, HALF_EARTH = 20037508.34;
    public static class Tile
    {
        public int x, y, z;
        
        public Tile(int x,int y,int z)
        {
            this.x=x; this.y=y; this.z=z;
        }

        public double getMetresInTile() {
            return EARTH/Math.pow(2,z);
        }

        public Point getBottomLeft() {
            double metresInTile = getMetresInTile();
            return new Point(x*metresInTile - HALF_EARTH, HALF_EARTH - (y+1)*metresInTile);    
        }

        public Point getTopRight() {
            Point p = getBottomLeft();
            double metresInTile = getMetresInTile();
            p.x += metresInTile;
            p.y += metresInTile;
            return p;    
        }
    }
    
    public Point project (Point lonLat)
    {
        return new Point(lonToGoogle(lonLat.x), latToGoogle(lonLat.y), lonLat.z);
    }
    
    public Point unproject (Point projected)
    {
        return new Point(googleToLon(projected.x),googleToLat(projected.y), projected.z);
    }
    
    private double lonToGoogle(double lon)
    {
        return (lon/180) * HALF_EARTH;
    }
    
    private double latToGoogle(double lat)
    {
        double y = Math.log(Math.tan((90+lat)*Math.PI/360)) / (Math.PI/180);
        return y*HALF_EARTH/180;
    }
    
    private double googleToLon(double x)
    {
            return (x/HALF_EARTH) * 180.0;
    }
    
    private double googleToLat(double y)
    {
        double lat = (y/HALF_EARTH) * 180.0;
        lat = 180/Math.PI * (2*Math.atan(Math.exp(lat*Math.PI/180)) - Math.PI/2);
        return lat;
    }
    
    public Tile getTile (Point p, int z)
    {
        Tile tile = new Tile(-1, -1, z);
        double metresInTile = tile.getMetresInTile(); 
        tile.x = (int)((HALF_EARTH+p.x) / metresInTile);
        tile.y = (int)((HALF_EARTH-p.y) / metresInTile);
        return tile;
    }
    
    public Tile getTileFromLonLat(Point lonLat, int z)
    {
        return getTile(new Point(lonToGoogle(lonLat.x),latToGoogle(lonLat.y)), z);
    }

    public String getID()
    {
        return "epsg:3857";
    }
}
