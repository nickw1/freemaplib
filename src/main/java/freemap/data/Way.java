package freemap.data;

import java.util.ArrayList;
import java.util.HashMap;
import freemap.jdem.DEM;
import java.io.PrintWriter;

public class Way extends Feature {

    public class DistInfo
    {
        public double distance;
        public int index;

        DistInfo(double d, int i) 
        {
            distance=d;
            index=i;
        }
    }

    ArrayList<Point> points;
    
    public Way()
    {
        points=new ArrayList<Point>();
    }
    
    public void addPoint(double x, double y)
    {
        points.add(new Point(x,y));
    }
    
    public void addPoint(double x, double y, double z)
    {
        points.add(new Point(x,y,z));
    }

    public void addPoint(Point p)
    {
        points.add(p);
    }
    
    public int nPoints()
    {
        return points.size();
    }
    
    public Point getPoint(int i)
    {
        return points.get(i);
    }
    
    public Point getUnprojectedPoint (int i)
    {
        return proj == null ? points.get(i): proj.unproject(points.get(i));
    }
    
    public String toString()
    {
        return "Way: " + points.toString() + "\n" + super.toString();
    }
    
    public void applyDEM(DEM dem)
    {
        for(Point p: points)
        {
            p.z = dem.getHeight(p.x,p.y,proj);
        }
    }
    
    public boolean isWithin(DEM dem)
    {
        for (Point p: points)
        {
            if(dem.pointWithin(p,proj))
                return true;
        }
        return false;
    }
    
    public void save(PrintWriter pw)
    {
        pw.println("<way>");
        for(Point p: points)
        {
            pw.println("<point x='" + p.x+ "' y='" + p.y+ "' z='" + p.z + "' />");
        }
        pw.println(tagsAsXML());
        pw.println("</way>");
    }
    
    // in whatever units the points are represented as
    public double length()
    {
        double len = 0.0;
        for(int i=0; i<points.size()-1; i++)
            len += points.get(i).distanceTo(points.get(i+1));
        return len;
    }
    
    public double distanceTo(Point p)
    {
        double smallestDistance = Double.MAX_VALUE,curDist;
        for(int i=0; i<points.size(); i++)
        {
            curDist = points.get(i).distanceTo(p);
            if(curDist<smallestDistance)
                smallestDistance=curDist;
        }
        return smallestDistance;    
    }
    
    // 130715 corrected to unproject way points when calculating distance
    public double haversineDistanceTo(Point p)
    {
        double smallestDistance = Double.MAX_VALUE, curDist;
        for(int i=0; i<points.size(); i++)
        {
    
            Point unproj = (proj==null) ? points.get(i): proj.unproject(points.get(i));
            curDist = Algorithms.haversineDist(p.x, p.y, unproj.x, unproj.y);
        
            if(curDist < smallestDistance)
                smallestDistance = curDist;
        }
        return smallestDistance;
    }

    public DistInfo findNearestPointTo(Point p)
    {
        double smallestDistance = Double.MAX_VALUE, curDist;
        int index=-1;
        for(int i=0; i<points.size(); i++)
        {
    
            curDist = points.get(i).distanceTo(p);
        
            if(curDist < smallestDistance) 
            {
                smallestDistance = curDist;
                index = i;
            }
        }
        return new DistInfo(smallestDistance, index);
    }
    
    public void reproject(Projection newProj)
    {
    
        for(int i=0; i<points.size(); i++)
        {
            points.set(i, (proj==null) ? points.get(i): proj.unproject(points.get(i)));
            points.set(i, (newProj==null) ? points.get(i): newProj.project(points.get(i)));
        }
        proj=newProj;
    }

    public ArrayList<Point> getPoints()
    {
        return points;
    }

    public void setPoints(ArrayList<Point> newPoints) {
        points = newPoints;
    }

    public double haversineDistanceTo(Way other) 
    {
        double minSoFar = Double.MAX_VALUE;
        for(Point p: other.points) 
        {
            double dist = haversineDistanceTo(p);
            if(dist < minSoFar) 
            {    
                minSoFar = dist;
            }
        }
        return minSoFar; 
    }

    public double distanceTo(Way other) 
    {
        double minSoFar = Double.MAX_VALUE;
        for(Point p: other.points) 
        {
            double dist = distanceTo(p);
            if(dist < minSoFar) 
            {    
                minSoFar = dist;
            }
        }
        return minSoFar; 
    }

    public Way getUnprojectedWay()
    {
        Way way = new Way();
        for(HashMap.Entry<String, String> tag: tags.entrySet()) 
        {
            way.addTag(tag.getKey(), tag.getValue());
        }
        for(Point p: points)
        {
            way.addPoint(proj==null ? new Point(p.x, p.y, p.z) : proj.unproject(p));
        }
        return way;
    }

    public BoundingBox getBoundingBox()
    {
        BoundingBox bbox = new BoundingBox(Double.MAX_VALUE, Double.MAX_VALUE, -Double.MAX_VALUE, -Double.MAX_VALUE);
        for(Point p: points) 
        {
            if(p.x < bbox.bottomLeft.x) {
                bbox.bottomLeft.x = p.x;
            }
            if(p.y < bbox.bottomLeft.y) {
                bbox.bottomLeft.y = p.y;
            }
            if(p.x > bbox.topRight.x) {
                bbox.topRight.x = p.x;
            }
            if(p.y > bbox.topRight.y) {
                bbox.topRight.y = p.y;
            }
        }
        return bbox;
    }
}
