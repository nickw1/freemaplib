package freemap.datasource;


/**
 * Created by nick on 07/07/15.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import freemap.data.Way;
import freemap.data.Point;

import freemap.data.*;

// 0101 now using TiledData; Tile is not very flexible e.g. not useful for an
// XYZ tile system

public class OSMTiles
{
    HashMap<String, TiledData> osmTiles;

    abstract class ProjectableIterator
    {
        Iterator<Map.Entry<String, TiledData>> tileIterator;
        FreemapDataset curTile;
        
        ProjectableIterator()
        {
            tileIterator = osmTiles.entrySet().iterator();
            if(tileIterator.hasNext())
            {
                curTile= (FreemapDataset)(tileIterator.next().getValue());
                initProjectableIterator();
               
            }
        }
        
        public Projectable next() 
        {
            if(!nullProjectableIterator() && projectableIteratorHasNext())
                return nextFeature();
            else if (tileIterator.hasNext())
            {
                Map.Entry<String, TiledData> e = tileIterator.next();
                //System.out.println("\n*** NEW TILE: " + e.getKey());
                curTile = (FreemapDataset)e.getValue();
                initProjectableIterator();
                if(projectableIteratorHasNext())
                    return nextFeature();
                else
                    return next(); // recursively call for next tile
            }
            else
                return null;
        }
        
        abstract void initProjectableIterator();
        abstract Projectable nextFeature();
        abstract boolean nullProjectableIterator();
        abstract boolean projectableIteratorHasNext();
        
    }
    
    public class WayIterator extends ProjectableIterator
    {
        Iterator<Way> projectableIterator;


        void initProjectableIterator()
        {
             projectableIterator = curTile.wayIterator();
        }
        
        Projectable nextFeature()
        {
            return projectableIterator.next();
        }
        
        boolean nullProjectableIterator()
        {
            return projectableIterator==null;
        }
        
        boolean projectableIteratorHasNext()
        {
            return projectableIterator.hasNext();
        }
    }

    abstract class PointIterator extends ProjectableIterator
    {
        Iterator<Long> projectableIterator;


        boolean nullProjectableIterator()
        {
            return projectableIterator==null;
        }
        
        boolean projectableIteratorHasNext()
        {
            return projectableIterator.hasNext();
        }
    }

    public class POIIterator extends PointIterator
    {
        void initProjectableIterator()
        {
            projectableIterator = curTile.poiIterator();
        }

        Projectable nextFeature()
        {
            return curTile.getPOIById(projectableIterator.next());
        }
    }

    public class AnnotationIterator extends PointIterator
    {
        void initProjectableIterator()
        {
            projectableIterator = curTile.annotationIterator();
        }

        Projectable nextFeature()
        {
            return curTile.getAnnotationById(projectableIterator.next());
        }

    }
        
    public OSMTiles()
    {
        osmTiles = new HashMap<String, TiledData>();
    }

    public OSMTiles(HashMap<String, TiledData> data) {
        osmTiles = data;

    }

    public void clear()
    {
        osmTiles.clear();
    }

    public HashMap<String, TiledData> tilesAsMap()
    {
        return osmTiles;
    }
    
    public WayIterator wayIterator()
    {
        return new WayIterator();
    }
    
    public POIIterator poiIterator()
    {
        return new POIIterator();
    }

    public AnnotationIterator annotationIterator()
    {
        return new AnnotationIterator();
    }

    public void operateOnNearbyPOIs(FreemapDataset.POIVisitor visitor, Point pointLL, double distanceMetres)
    {
        Iterator<Map.Entry<String, TiledData>> iterator = osmTiles.entrySet().iterator();
        while (iterator.hasNext())
            ((FreemapDataset)(iterator.next().getValue())).operateOnNearbyPOIs(visitor, pointLL, distanceMetres);
    }

    public void operateOnPOIs(FreemapDataset.POIVisitor visitor)
    {
        Iterator<Map.Entry<String, TiledData>> iterator = osmTiles.entrySet().iterator();
        while (iterator.hasNext())
        {
        
            ((FreemapDataset) (iterator.next().getValue())).operateOnPOIs(visitor);
        }
    }

    public void operateOnAnnotations(FreemapDataset.AnnotationVisitor visitor)
    {
        Iterator<Map.Entry<String, TiledData>> iterator = osmTiles.entrySet().iterator();
        while (iterator.hasNext())
        {
        
            ((FreemapDataset) (iterator.next().getValue())).operateOnAnnotations(visitor);
        }
    }

    public void operateOnWays(FreemapDataset.WayVisitor visitor)
    {
        Iterator<Map.Entry<String, TiledData>> iterator = osmTiles.entrySet().iterator();
        while (iterator.hasNext())
            ((FreemapDataset) (iterator.next().getValue())).operateOnWays(visitor);
    }

    public void operateOnNearbyWays(FreemapDataset.WayVisitor visitor, Point point, double distance)
    {
        Iterator<Map.Entry<String, TiledData>> iterator = osmTiles.entrySet().iterator();
        while (iterator.hasNext())
            ((FreemapDataset) (iterator.next().getValue())).operateOnNearbyWays(visitor, point, distance);
    }
    
    public ArrayList<Way> findWaysById(long id, boolean multiple)
    {
        ArrayList<Way> ways = new ArrayList<Way>();
        Iterator<Map.Entry<String,TiledData>> iterator = osmTiles.entrySet().iterator();
        while(iterator.hasNext())
        {
            Way currentWay = ((FreemapDataset)iterator.next().getValue()).getWayById(id);
            if(currentWay!=null) 
            {
                ways.add(currentWay);
                if(ways.size()==1 && !multiple) 
                {
                    return ways;
                }
            }
        }
        return ways;
    }

    public ArrayList<Way> findWaysById(long id)
    {
        return findWaysById(id, true);
    }

    public Way findWayById(long id)
    {
        ArrayList<Way> ways = findWaysById(id, false);
        return ways.size() == 1 ? ways.get(0) : null;
    }

    public POI findPOIById(long id)
    {
        Iterator<Map.Entry<String,TiledData>> iterator = osmTiles.entrySet().iterator();
        while(iterator.hasNext())
        {
            POI currentPOI = ((FreemapDataset)iterator.next().getValue()).getPOIById(id);
            if(currentPOI!=null) 
            {
                return currentPOI;
            }
        }
        return null;
    }

    static class TestVisitor implements FreemapDataset.POIVisitor, FreemapDataset.WayVisitor
    {
        public void visit(Way w)
        {
            System.out.println(w);
        }
        
        public void visit(POI poi)
        {
            System.out.println(poi);
        }
    }
}
