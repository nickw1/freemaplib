package freemap.datasource;

import freemap.data.Point;
import java.util.HashMap;

// Updates:
// 26/11/11 can now select desired ways or POIs
// 03/12/11 can now select annotations

public class FreemapFileFormatter extends FileFormatter{

    String projID, selectedWays, selectedPOIs, format;
    boolean doAnnotations;
    HashMap<String,String> keyvals;
    int tileWidth, tileHeight;
       boolean microdegToDeg = false; 
    
    public FreemapFileFormatter(String projID)
    {
        this(projID, "xml", 5000, 5000);
    }
    
    public FreemapFileFormatter(String projID, String format)
    {
        this(projID, format, 5000, 5000);
    }
    
    public FreemapFileFormatter(String projID, String format, int tileWidth, int tileHeight)
    {
        this.projID=projID;
        this.format=format;
        keyvals = new HashMap<String, String>();
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }
    
    public void selectWays(String waytypes)
    {
        selectedWays=waytypes;
    }
    
    public void selectPOIs(String poitypes)
    {
        selectedPOIs=poitypes;
    }
    
    public void selectAnnotations(boolean a)
    {
        doAnnotations=a;
    }
    
    public void unselectWays()
    {
        selectedWays=null;
    }
    
    public void unselectPOIs()
    {
        selectedPOIs=null;
    }
    
    public void addKeyval(String k, String v)
    {
        keyvals.put(k, v);
    }

    // 210218 allows conversion from microdeg to degrees
    public void setMicrodegToDeg(boolean md)
    {
        microdegToDeg = md;
    }
    
    public String format(Point bottomLeft)
    {
        Point tileBottomLeft = new Point();
		Point tileTopRight = new Point();
        tileBottomLeft.x = ( ((int)bottomLeft.x)/tileWidth ) * tileWidth;
        tileBottomLeft.y = ( ((int)bottomLeft.y)/tileHeight ) * tileHeight;
		tileTopRight.x = tileBottomLeft.x + tileWidth;
		tileTopRight.y = tileBottomLeft.y + tileHeight;
		if(microdegToDeg) {
			tileBottomLeft.x *= 0.000001;
			tileBottomLeft.y *= 0.000001;
			tileTopRight.x *= 0.000001;
			tileTopRight.y *= 0.000001;
		}
        String	url = "?bbox="+tileBottomLeft.x+","+tileBottomLeft.y+","+tileTopRight.x+","+ tileTopRight.y+ getFeatureTypes();
        return url;
    }
    
    public String getFeatureTypes()
    {
        String url = 
            (selectedWays==null?"":"&way="+selectedWays)+
            (selectedPOIs==null?"":"&poi="+selectedPOIs)+
            (doAnnotations==true?"&annotation=1":"")+
             "&format="+format+"&inProj="+projID.replace("epsg:","")+"&outProj=epsg:4326";
        for(java.util.Map.Entry<String,String> entry: keyvals.entrySet())
            url += "&" + entry.getKey() + "=" + entry.getValue();
        return url;
    }
}
